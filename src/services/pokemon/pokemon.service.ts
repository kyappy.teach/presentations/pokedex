import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';
import {IPokemon} from '../../models/i-pokemon';
import {IRestCollection} from '../../models/i-rest-collection';
import {map, switchAll} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private readonly _route: string = 'https://pokeapi.co/api/v2/pokemon';

  public constructor(private _HTTP: HttpClient) {}

  public details$(name: string): Observable<IPokemon> {
    return this._HTTP.get<IPokemon>(`${this._route}/${name}`);
  }

  public list$(): Observable<IPokemon[]> {
    return this._HTTP.get<IRestCollection>(this._route).pipe(
      map((restCollection: IRestCollection): Observable<IPokemon[]> =>
        forkJoin(this.restCollectionToPokemons$(restCollection))),
      switchAll()
    );
  }

  private restCollectionToPokemons$(restCollection: IRestCollection): Observable<IPokemon>[] {
    return restCollection.results
      .map((pokemon: { name: string }): Observable<IPokemon> => this.details$(pokemon.name));
  }
}
