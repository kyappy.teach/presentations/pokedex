export interface IPokemon {
  base_experience: number;
  height: number;
  id: number;
  is_default: boolean;
  name: string;
  order: number;
  sprites: {
    front_default: string;
  };
  weight: number;
}
