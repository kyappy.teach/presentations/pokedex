import {Component, Input} from '@angular/core';
import {IPokemon} from '../../models/i-pokemon';

@Component({
  selector: 'app-pokemon-grid',
  templateUrl: './pokemon-grid.component.html'
})
export class PokemonGridComponent {
  @Input() public pokemons: IPokemon[];
}
