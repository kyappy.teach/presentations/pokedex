import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './root.page.html'
})
export class RootPage {}
