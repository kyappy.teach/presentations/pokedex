import {Component} from '@angular/core';
import {IPokemon} from '../../models/i-pokemon';
import {PokemonService} from '../../services/pokemon/pokemon.service';

@Component({
  templateUrl: './pokemon-list.page.html'
})
export class PokemonListPage {
  public pokemons: IPokemon[];

  public constructor(private _POKEMON_SERVICE: PokemonService) {
    this._POKEMON_SERVICE.list$().subscribe((pokemons: IPokemon[]): void => {
      this.pokemons = pokemons;
    });
  }
}
