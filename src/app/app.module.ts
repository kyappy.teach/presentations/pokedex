import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RootPage } from '../pages/root/root.page';
import {PokemonListPage} from '../pages/pokemon-list/pokemon-list.page';
import {PokemonGridComponent} from '../components/pokemon-grid/pokemon-grid.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    RootPage,
    PokemonListPage,
    PokemonGridComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [RootPage]
})
export class AppModule { }
