import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PokemonListPage} from '../pages/pokemon-list/pokemon-list.page';

const routes: Routes = [
  {
    component: PokemonListPage,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
