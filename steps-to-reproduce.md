# Angular tutorial - Pokédex

## Table of content
1. [Initial](#initial)
2. [Bootstrap](#bootstrap)
3. [Fix app](#fix-app)
4. [Create root page](#create-root-page)
5. [Create Pokémon List page](#create-pokémon-list-page)
6. [Register Pokémon List page in routes](#register-pokémon-list-page-in-routes)
7. [Add Pokémon grid component](#add-pokémon-grid-component)
8. [Add Pokémon model](#add-pokémon-model)
9. [Add Pokémon service](#add-pokémon-service)
10. [Get Pokémons form Pokémon service](#get-pokémons-form-pokémon-service)
11. [Display Pokémons in grid](#display-pokémons-in-grid)

## Initial

- `ng new pokedex`
- set `start` script in WebStorm
- Disable tslint

## Bootstrap

- Go to `package.json`
- `yarn add bootstrap`
- See packkage added
- Explain node_modules folder
- Go to `style.scss`
- Explains `scss`
- Add `@import "~bootstrap/scss/bootstrap";`

## Fix app

- In `app.component.html`

```html
<div class="bg-light">
  <nav class="navbar navbar-light bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Pokédex</a>
  </nav>
  <div class="container-fluid pt-3">
    Hello World!
  </div>
</div>
```
- Remove `title` property in `app.component.ts`
- Remove `styleUrls: ['./app.component.scss']` component configuration in `app.component.ts`
- Remove `app.component.scss` file.

## Create root page

- Create `pages` folder in `src` folder
- Create `root` folder in `pages` folder
- Move app component in `pages/root` folder
- Rename `app.component.ts` to `root.page.ts`
- Fix linter for `app.module`

## Create Pokémon List page

- Create `pokemon-list` folder in `pages` folder
- Create `pokemon-list.page.ts` file folder in `pages/pokemon-list` folder
- Create `pokemon-list.page.html` file folder in `pages/pokemon-list` folder
- In `pokemon-list.page.ts`

```TypeScript
import {Component} from '@angular/core';  

@Component({
  templateUrl: './pokemon-list.page.html'
})
export class PokemonListPage {}
```

- In `pokemon-list.page.html`, replace content with `Pokémon list page`
- Register `PokemonListPage` in `AppModule` `declarations`

## Register Pokémon List page in routes

- In `app-routing.module`, add the following object in `routes` array

```TypeScript
{
  component: PokemonListPage,
  path: ''
}
```

- In `root.page.html`, replace `Hello World!` with `<router-outlet></router-outlet>`
- You can see that `pokemon-list.page` content is now rendered inside root page.

## Add Pokémon grid component

- Create `components` folder in `src` folder
- Create `pokemon-grid` folder in `src/components` folder
- Create `pokemon-grid.component.ts` file folder in `components/pokemon-grid` folder
- Create `pokemon-grid.component.html` file folder in `components/pokemon-grid` folder
- In `pokemon-grid.component.ts`

```TypeScript
import {Component} from '@angular/core';

@Component({
  selector: 'app-pokemon-grid',
  templateUrl: './pokemon-grid.component.html'
})
export class PokemonGridComponent {}
```

- In `pokemon-grid.component.html`, replace content with `Pokémon grid component`
- Register `PokemonGridComponent` in `AppModule` `declarations`
- In `pokemon-list.page.html`, replace content with `<app-pokemon-grid></app-pokemon-grid>`
- You can see that `pokemon-grid.component` content is now rendered inside pokemon list page.

## Add Pokémon model

- Create `models` folder in `src` folder
- Create `i-rest-collection.ts` file in `src/models` folder
- In `i-rest-collection.ts`

```TypeScript
export interface IRestCollection {
  count: number;
  next: string;
  previous: string;
  results: {
    name: string;
    url: string;
  }[];
}

```

- Create `i-pokemon.ts` file in `src/models` folder
- In `i-pokemon.ts`

```TypeScript
export interface IPokemon {
  base_experience: number;
  height: number;
  id: number;
  is_default: boolean;
  name: string;
  order: number;
  sprites: {
    front_default: string;
  };
  weight: number;
}
```

## Add Pokémon service

- In `app.module.ts`, add `HttpClientModule` to `imports` array
- Create `services` folder in `src` folder
- Create `pokemon` folder in `src/services` folder
- Create `pokemon.service.ts` file in `service/pokemon` folder
- In `pokemon.service.ts`

```TypeScript
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {forkJoin, Observable} from 'rxjs';
import {map, switchAll} from 'rxjs/operators';
import {IPokemon} from '../../models/i-pokemon';
import {IRestCollection} from '../../models/i-rest-collection';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private readonly _route: string = 'https://pokeapi.co/api/v2/pokemon';

  public constructor(private _HTTP: HttpClient) {}

  public details$(name: string): Observable<IPokemon> {
    return this._HTTP.get<IPokemon>(`${this._route}/${name}`);
  }

  public list$(): Observable<IPokemon[]> {
    return this._HTTP.get<IRestCollection>(this._route).pipe(
      map((restCollection: IRestCollection): Observable<IPokemon[]> =>
        forkJoin(this.restCollectionToPokemons$(restCollection))),
      switchAll()
    );
  }

  private restCollectionToPokemons$(restCollection: IRestCollection): Observable<IPokemon>[] {
    return restCollection.results
      .map((pokemon: { name: string }): Observable<IPokemon> => this.details$(pokemon.name));
  }
}
```

## Get Pokémons form Pokémon service

- In `pokemon-list.page.ts`, add the following code in class body:

```TypeScript
public pokemons: IPokemon[];

public constructor(private _POKEMON_SERVICE: PokemonService) {
  this._POKEMON_SERVICE.list$().subscribe((pokemons: IPokemon[]): void => {
    this.pokemons = pokemons;
  });
}
```

- In `pokemon-grid.component.ts`, add the following code in class body:

```TypeScript
@Input() public pokemons: IPokemon[];
```

- In `pokemon-grid.component.html`, replace the content with:

```html
<pre>{{ pokemons | json }}</pre>
```

- In `pokemon-list.page.html`:

```html
<app-pokemon-grid [pokemons]="pokemons"></app-pokemon-grid>
```

## Display Pokémons in grid

- In `pokemon-grid.component.html`, replace the content with:

```html
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col"></th>
      <th scope="col">#</th>
      <th scope="col">Order</th>
      <th scope="col">Name</th>
      <th scope="col">Base experience</th>
      <th scope="col">Height</th>
      <th scope="col">Weight</th>
    </tr>
  </thead>
  <tbody>
    <tr *ngFor="let pokemon of pokemons">
      <td class="align-middle" width="65"><img height="48px" [src]="pokemon.sprites.front_default" [attr.alt]="pokemon.name"></td>
      <th class="align-middle" scope="row">{{ pokemon.id }}</th>
      <td class="align-middle">{{ pokemon.order }}</td>
      <td class="align-middle">{{ pokemon.name | titlecase }}</td>
      <td class="align-middle">{{ pokemon.base_experience }}</td>
      <td class="align-middle">{{ pokemon.height * .1 | number:'1.1-1' }} m</td>
      <td class="align-middle">{{ pokemon.weight * .1 | number:'1.1-1' }} kg</td>
    </tr>
  </tbody>
</table>
```

- In `pokemon-list.page.html`, replace the content with:

```html
<div class="card my-3 shadow">
  <div class="card-body">
    <h1>Pokémon list</h1>
  </div>
  <app-pokemon-grid [pokemons]="pokemons"></app-pokemon-grid>
</div>
```
