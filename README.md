# Pokedex

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Steps to reproduce

You can follow the [steps to reproduce](./steps-to-reproduce.md) to make your own version of the presentation.

## References

- [Angular Documentation](https://angular.io/)
- [Bootstrap Documentation](https://getbootstrap.com/)
- [PokéAPI - The RESTful Pokémon API](https://pokeapi.co/)
- [Just Finished a Paid Career Path, Am I a Web Developer Now?](https://levelup.gitconnected.com/just-finished-a-paid-career-path-am-i-a-web-developer-now-85ff53b7580c)
- [The Official Pokémon Website](https://www.pokemon.com/)
